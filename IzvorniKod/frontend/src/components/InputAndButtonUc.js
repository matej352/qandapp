import React, { createContext } from 'react'
import CurrentUserContext from './store/CurrentUserContext';
import { useState, useContext } from 'react';
import { Formik } from 'formik';
import { useFormik} from "formik";
import * as Yup from 'yup'

export default function InputAndButton(props) {

    const ctx = useContext(CurrentUserContext);

    const formik = useFormik({
        initialValues:{
            odgovor: ""
        },
        validationSchema: Yup.object({
            //donorId: Yup.string().required("Molimo unesite donorov ID") //number
        }),
        onSubmit: (e) => {
                props.submitFunc(formik.values.odgovor);
            }
    })
    



    return (
        <div>
            <form onSubmit={ (e) =>{ e.preventDefault(); formik.handleSubmit(e)  }}>
                Odgovor:
                <div>
                    <input type="text"
                        id="odgovor"
                        name="odgovor"
                        type="text"
                        placeholder="Odgovor"
                        value={formik.values.odgovor}
                        onChange={formik.handleChange}
                    />
                </div>
                <div>
                    <button type="submit">Pošalji</button>
                </div>
            </form>
        </div>
    )
}
