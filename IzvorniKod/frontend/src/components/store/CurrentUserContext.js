import { createContext } from "react";
import { useState } from "react";

const CurrentUserContext = createContext({
    currentUser: "",
    room: "",
    uloga: "",
    questions: [],
    setCurrentUser: (newCurrentUser) => {},
    setRoom: (newRoom) => {},
    setUloga: (newUloga) => {},
    setQuestions: (asdasda) => {}
});

export function CurrentUserContextProvider(props) {
    const [thisCurrentUser, setThisCurrentUser] = useState("");  
    const [thisRoom, setThisRoom] = useState("");
    const [thisUloga, setThisUloga] = useState("");
    const [thisQuestions, setThisQuestions] = useState([]);

    function setThisCurrentUserHandler(user){
        setThisCurrentUser(user);
    }

    function setThisRoomHandler(room){
        setThisRoom(room);
    }

    function setThisUlogaHandler(uloga){
        setThisUloga(uloga);
    }

    const context = {
        currentUser: thisCurrentUser,
        room: thisRoom,
        uloga: thisUloga,
        questions: thisQuestions,
        setCurrentUser: setThisCurrentUserHandler,
        setRoom: setThisRoomHandler,
        setUloga: setThisUlogaHandler,
        setQuestions: setThisQuestions
    }

    return <CurrentUserContext.Provider value={context}>
            {props.children}
        </CurrentUserContext.Provider>
    
}

export default CurrentUserContext; 