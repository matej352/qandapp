import React, { createContext } from 'react'
import CurrentUserContext from './store/CurrentUserContext';
import { useState, useContext } from 'react';
import { Formik } from 'formik';
import { useFormik} from "formik";
import * as Yup from 'yup'

export default function InputAndButtonProf(props) {

    const ctx = useContext(CurrentUserContext);

    const formik = useFormik({
        initialValues:{
            pitanje: ""
        },
        validationSchema: Yup.object({
            //donorId: Yup.string().required("Molimo unesite donorov ID") //number
        }),
        onSubmit: (e) => {
                props.submitFunc(formik.values.pitanje);
            }
    })
    



    return (
        <div>
            <form onSubmit={ (e) =>{ e.preventDefault(); formik.handleSubmit(e)  }}>
                Unesite tekst pitanja:
                <div>
                    <input type="text"
                        id="pitanje"
                        name="pitanje"
                        type="text"
                        placeholder="Pitanje"
                        value={formik.values.pitanje}
                        onChange={formik.handleChange}
                    />
                </div>
                <div>
                    <button type="submit">Pošalji</button>
                </div>
            </form>
        </div>
    )
}
