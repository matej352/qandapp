import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {CurrentUserContextProvider} from './components/store/CurrentUserContext'


ReactDOM.render(
  <CurrentUserContextProvider>
    <BrowserRouter>
        <App/>
    </BrowserRouter>
  </CurrentUserContextProvider>,
                  document.getElementById('root')
);

