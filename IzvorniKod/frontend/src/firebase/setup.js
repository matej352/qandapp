import firebase from "firebase/compat/app";
import "firebase/compat/storage";


const firebaseConfig = {
  apiKey: "AIzaSyBM9QV6jC1odz-fcm04ouNaUgE1Nhg-Mys",
  authDomain: "qandapp-ef452.firebaseapp.com",
  projectId: "qandapp-ef452",
  storageBucket: "qandapp-ef452.appspot.com",
  messagingSenderId: "78331328701",
  appId: "1:78331328701:web:112adffdfb5e0095ab9fd4",
  measurementId: "G-ZTYLQ753M2"
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { storage, firebase as default };