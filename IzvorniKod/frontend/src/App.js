import Home from "./pages/home";
import Profesor from "./pages/profesor";
import Ucenik from "./pages/ucenik";
import Odgovori from "./pages/odgovori";
import Prikaz from "./pages/prikaz";
import {Route, Routes} from "react-router-dom"
import SockJsClient from 'react-stomp'
import { useState } from "react";



function App() {
  return (
            <Routes>
              <Route path='/' element={<Home/>} /> 
              <Route path='/odgovori' element={<Odgovori/>} />
              <Route path='/prikazi' element={<Prikaz/>} />
              <Route path='/ucenik' element={<Ucenik/>}/>
              <Route path='/profesor' element={<Profesor/>}/>
            </Routes>
  );
}

export default App;
