import React from 'react';
import {Link} from 'react-router-dom';
import { useFormik } from "formik";
import * as Yup from 'yup';
import CurrentUserContext from "../components/store/CurrentUserContext";
import { useContext } from "react";
import { Navigate, useNavigate } from 'react-router';
import { w3cwebsocket as W3CWebSocket } from "websocket";

export default function Home() {

    const navigate = useNavigate();
    const context = useContext(CurrentUserContext);

    const formik = useFormik({
        initialValues:{
            uloga: "",
            user: "",
            soba: ""
        },
        validationSchema: Yup.object({
            user: Yup.string().required("Molimo unesite username"),
            soba: Yup.string().required("Molimo unesite sobu")
        }),
        onSubmit: (e) => {
            context.setCurrentUser(formik.values.user);
            context.setRoom(formik.values.soba);
            context.setUloga(formik.values.uloga);

            /*const client = new W3CWebSocket("ws://localhost:8080/app/chat.askQuestion/" + formik.values.soba);
            
            client.send(JSON.stringify({
                type: "TEXT_QUESTION",
                content: "badsda" ,
                sender: formik.values.user,
                room : formik.values.soba
              }));*/
         


            if(formik.values.uloga=="profesor"){
                
                navigate("/profesor");
            }
            else{
                navigate("/ucenik");
            }
        }
    });

    function radioHandleChangeProf(){
        formik.values.uloga="profesor";
    }

    function radioHandleChangeUc(){
        formik.values.uloga="ucenik";
    }

    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <div>
                    username
                    <input 
                        type="text" 
                        name="user" 
                        id="user"  
                        placeholder='unesi username'
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.user}
                    />
                </div>
                {formik.errors.user && formik.touched.user ? <p>{formik.errors.user}</p> : null}
                <div>
                    soba
                    <input 
                        type="text" 
                        name="soba"
                        id="soba"
                        placeholder='unesi sobu'
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.soba}
                    />
                </div>
                {formik.errors.soba && formik.touched.soba ? <p>{formik.errors.soba}</p> : null}
                <div>
                    <label>
                        Profesor
                        <input
                            id="profesor"
                            name="uloga"
                            type="radio"
                            value="profesor"
                            onChange={formik.handleChange}
                            onClick={radioHandleChangeProf}
                        />
                    </label>
                    <label>
                        Učenik
                        <input
                            id="ucenik"
                            name="uloga"
                            type="radio"
                            value="ucenik"  
                            onChange={formik.handleChange}
                            onClick={radioHandleChangeUc}
                            defaultChecked
                        />
                    </label>
                </div>  
                <div>
                    <button type="submit">Dalje</button>
                </div>
            </form>
        </div>
    )
}
