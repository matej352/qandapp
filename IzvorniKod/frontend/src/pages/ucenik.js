import React from 'react';
import {useState, useContext} from 'react';
import InputAndButtonUc from '../components/InputAndButtonUc';
import CurrentUserContext from '../components/store/CurrentUserContext';

function Ucenik() {
    const ctx = useContext(CurrentUserContext);
    const ws = new WebSocket("ws://localhost:8080");
    //const [questionText, setQuestionText] = useState('');
    //var questionText1="";
    const [questionAnswer, setQuestionAnswer] = useState('');

    var id=0;
    
    ws.onopen = () => {
        console.log("connected");
        //ws.send(JSON.stringify({event:'ucenikTest'}));
    }
    
    ws.onmessage = (e) => {
        var msg = JSON.parse(e.data); //msg je naš objekt tj. {event:'',data:''}
        //console.log("Stigla poruka: " + msg.event + ":" + msg.data);

        if (msg.event==='questionText'){
            ctx.setRoom(msg.data.txt);  // data: {id:0, txt:'Pitanje'}
        } 
    }
    
    ws.onclose = () => {
        console.log("disconnected");
    }

    const sendAnswer = (event) => {
        id=id+1;
        ws.send(JSON.stringify({event:'answerText', data: {"id": id,'txt':event}}));
    }

    return (
        <div>
            Ucenikova stranica<br/>
            <div>Pitanje: {ctx.room}</div>

            <InputAndButtonUc submitFunc={sendAnswer}/>

        </div>
    )
}

export default Ucenik;