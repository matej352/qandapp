import React, {useState} from 'react'
import SockJsClient from 'react-stomp'
import { Link } from 'react-router-dom';


export default function Odgovori() {

    const SOCKET_URL = 'http://localhost:8080/ws';
    const [message, setMessage] = useState('You server message here.');
  
    let onMessageReceived = (msg) => {
      setMessage(msg.content);
      console.log(msg.content)
    }

    function newTabOpener(){
        window.open("http://localhost:3000/prikazi", "width=200","height=100")
    }
    
    return (
        <div>
            <SockJsClient
                url={SOCKET_URL}
                topics={['/topic/123/answers']}
                onConnect={console.log("Connected!")}
                onDisconnect={console.log("Disconnected!2")}
                onMessage={msg => onMessageReceived(msg)}
                debug={false}
             />
             Odgovori:
             {message}
            <div>
                <button onClick={newTabOpener}>Pokaži svima</button>
            </div>
        </div>
    )
}
