import React, {useState, useContext} from 'react'
import InputAndButtonProf from '../components/InputAndButtonProf';
import InputAndButton from '../components/InputAndButtonProf';
import CurrentUserContext from '../components/store/CurrentUserContext';

export default function Profesor() {

    const ctx = useContext(CurrentUserContext);

    const ws = new WebSocket("ws://localhost:8080");

    var id = 0;
    
    //lista pitanja koja prof postavlja
    //const [questions, setQuestions] = useState([]);
    //const [questionAnswers, setQuestionAnswers] = useState([]);

    ws.onopen = () => {
        ws.send(JSON.stringify({event:'teacherConnected'}));
    }

    ws.onclose = () => {
        ws.send(JSON.stringify({event:'teacherDisconnected'}));
    }
    
    ws.onmessage = (e) => {
        var msg = JSON.parse(e.data); //msg je naš objekt tj. {event:'',data:''}
        console.log("Stigla poruka: " + msg.event + ":" + msg.data);
        if (msg.event==='answerText'){
            ctx.questions.push({"id":ctx.questions.length,'txt':msg.data.txt, 'answers':[]});
            ctx.setQuestions([...ctx.questions]);
            //setQuestionAnswers(msg.data);
        }
    }

    const [image, setImage] = useState(null);
    const [input, setInput] = useState('');

    const handleInputFileChange = (e) => {
        if(e.target.files[0]){
            setImage(e.target.files[0]);
        }
    }

    const submitQuestion = (input) => {
        id=id+1;
        console.log(input);
        var q = {"id":id,'txt':input};
        //questions.push({"id":questions.length,'txt':input, 'answers':[]});
        //setQuestions([...questions]);
        //pošalji pitanje na server
        ws.send(JSON.stringify({event:'questionText', data:q}));
    }

    return (
        <div>
            <InputAndButtonProf submitFunc={submitQuestion}/>
           
            <div>ODGOVORI:
                <ul>
                {ctx.questions.map((answer) => (
                    <li key={answer.id}>odgovor: {answer.txt}</li>
                ))}
                </ul>
            </div>
        </div>
    )
}
