import { WebSocketServer } from "ws";

const wss = new WebSocketServer({ port: 8080 });
console.log("Server started at 8080");

//jedan objekt za nastavnika
var teacher;
//lista svih aktivnih ucenika
var pupils = [];

function getDate() {
  var now = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
  return `[${now}]`;
}

wss.on("connection", function connection(ws, req) {
  var ip = req.socket.remoteAddress;

  console.log(getDate() + ` Client connected           [id=${ws.id}, ip=${ip}] `);

  ws.send(JSON.stringify({event:"server test"}));

  ws.on("disconnect", function () {
    console.log(
      getDate() +
        ` Client disconnected        [id=${ws.id}, ip=${ws.socket.remoteAddress}] `
    );
  });

  ws.on("message", function (receivedString) {
    console.log(getDate() + ` Client sent data: ${receivedString}`);
    var msg = JSON.parse(receivedString);

    //msg: {event:'event', data:'nekisadržaj'}

    //proba
    if (msg.event == "test") {
      console.log(getDate() + ` Test message received`);
    } 
    //handlanje stvarnih poruka
    else {
      switch (msg.event) {
        case "teacherConnected":
          break;
        case "questionText":
          console.log(getDate() + ` Question received: ${msg.data}`);
          //pošalji svim klijentima osim prof
          wss.clients.forEach(function each(client) {
            if (client !== ws && client.readyState === ws.OPEN) {
              client.send(JSON.stringify({event:"questionText", data: msg.data}));
            }
          });
          break;
        case "answerText":
          console.log(getDate() + ` Answer received: ${msg.data.txt}`);
          wss.clients.forEach(function each(client) {
            if (client !== ws && client.readyState === ws.OPEN) { 
              client.send(JSON.stringify({event:"answerText", data: msg.data}));
            }
          })
          break;
        default:
          console.log(getDate() + ` Unknown event received: ${msg}`);
      }
    }
  });

  /*ws.on('teacherConnected', function (data) {
    console.log(` *** teacherConnected ${data.msgtype}`);
    //ws.broadcast.emit("teacherConnected", data);
  });

  ws.on('questionText', function (data) {
    console.log(` *** questionText ${data.msgtype}`);
    ws.broadcast.emit("questionText", data);
  });

  ws.on('questionImage', function (data) {    
    console.log(` *** questionImage `);
    ws.broadcast.emit("questionImage", data);
  });

  ws.on('answerText', function (data) { 
    console.log(` *** answerText `);
    ws.broadcast.emit("answerText", data);
  });

  ws.on('answerImage', function (data) {
    console.log(` *** answerImage `);
    ws.broadcast.emit("answerImage", data);
  });*/
});
